import React from "react";
import {
  render,
  cleanup,
  waitForElement,
  wait,
  fireEvent,
} from "@testing-library/react";

import { StateProvider } from "utils/custom-hooks/global-state";
import reducer from "basket/store/reducer";

import { mockCrossSellEvents } from "utils/testing/mock-events";
import { mockStateBasket } from "utils/testing/mock-states";
import { mockBundle, mockCurrency } from "utils/testing/mock-basket";

import App from "basket/App";

let reserveBasket_promise = new Promise(function(resolve, reject) {
  resolve({});
});

let getConfig_promise = new Promise(function(resolve, reject) {
  resolve({
    raven: {},
  });
});

let getLabels_promise = new Promise(function(resolve, reject) {
  resolve({});
});

let getComponents_promise = new Promise(function(resolve, reject) {
  resolve({ ...mockStateBasket.app.components });
});

let getBasket_promise = new Promise(function(resolve, reject) {
  resolve({
    status: "",
    trolley: {
      bundles: [mockBundle],
    },
  });
});

let getEvents_promise = new Promise(function(resolve, reject) {
  resolve({events: mockCrossSellEvents});
});

const promise = new Promise(function(resolve, reject) {
  resolve({
    getComponents: () => getComponents_promise,
    getBasket: () => getBasket_promise,
    getConfig: () => getConfig_promise,
    getLabels: () => getLabels_promise,
    getEvents: () => getEvents_promise,
    reserveBasket: () => reserveBasket_promise,
  });
});

afterEach(cleanup);

let initialState = {};

describe("App", () => {
  beforeEach(() => {
    initialState = {
      app: {
        webWorker: { call: () => promise },
        components: {},
      },
      basket: { ...mockStateBasket.basket, trolley_token: "U-123" },
      popup_module: { ...mockStateBasket.popup_module },
    };
  });
  afterEach(cleanup);

  it("will render nothing when data is not ready", () => {
    const { container } = render(
      <StateProvider initialState={initialState} reducer={reducer}>
        <App />
      </StateProvider>
    );
    expect(container.firstChild).toBeNull();
  });

  it("always loads the core components of the basket page", async () => {
    getBasket_promise = new Promise(function(resolve, reject) {
      resolve({
        status: "",
        trolley: {
          meta: {
            currencies: { ...mockCurrency },
          },
          bundles: [{ ...mockBundle }],
        },
      });
    });

    const { getByText } = render(
      <StateProvider initialState={initialState} reducer={reducer}>
        <App />
      </StateProvider>
    );
    //header
    // await waitForElement(() => getByText("Basket"));
    //trolley
    await waitForElement(() => getByText("Basket Summary"));
    await waitForElement(() => getByText(mockBundle.events[0].description));
    // CTA
    // await waitForElement(() => getByText("Proceed to Checkout"));
    // footer
    // await waitForElement(() => getByText("Contact Us"));
  });

  //child component integration tests

  it("allows a user to reserve tickets if their timer runs outs", async () => {
    getBasket_promise = new Promise(function(resolve, reject) {
      resolve({
        trolley: {
          meta: {
            currencies: { ...mockCurrency },
          },
          bundles: [{ ...mockBundle }],
        },
        minutes_left: 0.02,
        status: "reserved",
        reserved_at: new Date(Date.now()),
      });
    });

    const { getByText } = render(
      <StateProvider initialState={initialState} reducer={reducer}>
        <App />
      </StateProvider>
    );
    await wait(() => {
      fireEvent.click(getByText("Reserve again"));
    });
  });
});
