# Slides for VueJS talk built with Reveal.js

## Installation

```
git clone https://gitlab.com/stevgouws/vuejs-talk-slides.git
npm install
```

## Run

`npm start`